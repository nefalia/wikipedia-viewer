//never gonna give you up, never gonna let you down, never gonna run around and desert you

$(document).ready(function(){ //wait until fully loaded

  $("#submitSearch").on("click", function(){ //when search button is clicked

    var searchval = document.getElementById("searchInput").value; //get user input

    //ask Wikipedia for searched items
    var askwiki = $.getJSON("http://en.wikipedia.org/w/api.php?action=query&list=search" + searchval + "&limit=10&format=json&callback=?", function(data){

      console.log(data);
    })

      .done(function(data) { //when search is done

        $(".searchresult").append("<p>Showing results for " + searchval + "</p>"); //tell user which results you are showing

        $.each(data.query, function(i, item) {

          var resultarray = []; //how about an array

          for (var i in item) { //in which every item and corresponding value is found

              var resultitem = "<div>" + data[i].title + "</div>"; //and pushing this format out

              array.push(resultitem); //and placing them in a new array
          };

          for (var i = 0; i < 9; i++) { //show at max 10 results bc reasons

            $(".searchresult").append(resultarray); //append the results to the DOM
          };
        });
      })
        .fail(function() {
        $(".searchresult").append("Something went wrong. Please try again.");
        console.log( "error" );
      })
        .always(function() {
        console.log( "complete" );
      });

    $("#search").append("<button>Reset</button>") //show reset button
    $(".button.random").remove(); //remove random page link
    $("#submitSearch").remove(); //remove search button
  });
});
